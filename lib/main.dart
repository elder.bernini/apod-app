import 'package:apod_app/modules/picture/presentation/screens/picture_list_screen.dart';
import 'package:apod_app/shared/design/colors/app_colors.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'APOD',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: primary),
        useMaterial3: true,
      ),
      home: const PictureListScrren(),
    );
  }
}
