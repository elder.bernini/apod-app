import 'package:apod_app/modules/picture/data/models/picture_local_model.dart';
import 'package:apod_app/shared/data/local_database/local_datasource.dart';
import 'package:isar/isar.dart';
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart';

class IsarLocalDatasource implements LocalDatasource<Isar> {
  final _lock = Lock();

  @override
  Future<Isar> getInstance() async {
    if (Isar.instanceNames.isEmpty) {
      var dir = await getApplicationDocumentsDirectory();
      return await _lock.synchronized(() async {
        return await Isar.open(
          [PictureLocalModelSchema],
          directory: dir.path,
        );
      });
    }

    return Future.value(Isar.getInstance());
  }

  @override
  Future<void> close() async {
    var isar = await getInstance();
    await isar.close();
  }

  @override
  Future<List<T>> getAll<T>() async {
    var isar = await getInstance();
    return isar.collection<T>().where().findAll();
  }

  @override
  Future<List<T>> getAllPaginated<T>(int limit, int offset) async {
    var isar = await getInstance();
    return isar.collection<T>().where().offset(offset).limit(limit).findAll();
  }

  @override
  Future<void> insert<T>(T model) async {
    var isar = await getInstance();
    return isar.writeTxn(() => isar.collection<T>().put(model));
  }

  @override
  Future<void> insertAll<T>(List<T> models) async {
    var isar = await getInstance();
    return isar.writeTxn(() => isar.collection<T>().putAll(models));
  }

  @override
  Future<void> removeAll<T>() async {
    var isar = await getInstance();
    return isar.writeTxn(() => isar.collection<T>().clear());
  }
}
