abstract class LocalDatasource<DB> {
  Future<DB> getInstance();
  Future<void> close();
  Future<List<T>> getAll<T>();
  Future<List<T>> getAllPaginated<T>(int limit, int offset);
  Future<void> insert<T>(T model);
  Future<void> insertAll<T>(List<T> model);
  Future<void> removeAll<T>();
}
