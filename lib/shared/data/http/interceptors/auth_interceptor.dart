import 'package:apod_app/shared/data/http/api_constans.dart';
import 'package:dio/dio.dart';

class AuthInterceptor extends Interceptor {
  @override
  Future<void> onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.queryParameters.addAll(
      {
        'api_key': apiKey,
      },
    );

    handler.next(options);
  }
}
