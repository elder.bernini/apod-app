import 'package:apod_app/shared/data/http/api_constans.dart';
import 'package:apod_app/shared/data/http/interceptors/auth_interceptor.dart';
import 'package:apod_app/shared/logger/logger.dart';
import 'package:dio/dio.dart';

import '../interceptors/error_interceptor.dart';
import '../interceptors/info_interceptor.dart';

Dio getDio() {
  final options = BaseOptions(
    baseUrl: apiBaseUrl,
    connectTimeout: const Duration(minutes: 1),
    receiveTimeout: const Duration(minutes: 1),
  );
  final dio = Dio(options);

  dio.interceptors
    ..add(AuthInterceptor())
    ..add(ErrorInterceptor())
    ..add(InfoInterceptor(log: LoggerImpl()));
  return dio;
}
