class Failure {
  String message;

  Failure({
    required this.message,
  });

  @override
  String toString() {
    return 'Failure: $message';
  }
}

class DatasourceFailure extends Failure {
  static const String mensagemErroGenerica = 'Unable to complete the request.';

  DatasourceFailure({
    String? message,
  }) : super(message: message ?? '$mensagemErroGenerica. Unexpected failure');
}

class WithoutConnectionFailure extends DatasourceFailure {
  WithoutConnectionFailure()
      : super(
          message:
              '${DatasourceFailure.mensagemErroGenerica}. Check your connection and try again',
        );
}

class UnnauthorizedFailure extends DatasourceFailure {
  UnnauthorizedFailure({String? message})
      : super(
          message: message ??
              '${DatasourceFailure.mensagemErroGenerica} Unauthorized',
        );
}

class BadRequestFailure extends DatasourceFailure {
  BadRequestFailure({String? message})
      : super(
          message: message ??
              '${DatasourceFailure.mensagemErroGenerica} Invalid request',
        );
}

class NotFoundRequestFailure extends DatasourceFailure {
  NotFoundRequestFailure()
      : super(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Resource not founded',
        );
}

class TooManyRequestsFailure extends DatasourceFailure {
  TooManyRequestsFailure({String? message})
      : super(
          message: message ??
              '${DatasourceFailure.mensagemErroGenerica} Unauthorized',
        );
}

class TimeoutFailure extends DatasourceFailure {
  TimeoutFailure()
      : super(
          message:
              '${DatasourceFailure.mensagemErroGenerica} Exceeded limit time',
        );
}
