import 'package:intl/intl.dart';

extension DateExtension on DateTime {
  String toStringFormated() {
    return DateFormat('MMM dd yyyy').format(this);
  }

  String toISO8601Date() {
    return DateFormat('yyyy-MM-dd').format(this);
  }
}
