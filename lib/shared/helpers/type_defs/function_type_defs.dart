typedef FromJsonFactory<R> = R Function(dynamic response);
typedef ToJsonFactory = dynamic Function();

typedef VoidTypedCallback<T> = void Function(T value);
