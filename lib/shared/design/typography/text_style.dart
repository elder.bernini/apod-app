import 'package:apod_app/shared/design/colors/app_colors.dart';
import 'package:flutter/widgets.dart';

const pRegular = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.normal,
  color: font,
  height: 1.2,
);
const p2Regular = TextStyle(
  fontSize: 14,
  color: font,
  fontWeight: FontWeight.normal,
  height: 1.1,
);
