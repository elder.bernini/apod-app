import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:isar/isar.dart';

part 'picture_local_model.g.dart';

@collection
class PictureLocalModel {
  final Id id = Isar.autoIncrement;
  final String title;
  final DateTime date;
  final String explanation;
  final String url;
  final String? hdUrl;

  PictureLocalModel({
    required this.title,
    required this.date,
    required this.explanation,
    required this.url,
    this.hdUrl,
  });

  Picture toDomain() {
    return Picture(
      title: title,
      date: date,
      explanation: explanation,
      url: url,
      hdUrl: hdUrl,
    );
  }
}
