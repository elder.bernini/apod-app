import 'package:apod_app/modules/picture/data/models/picture_local_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'picture_json_model.g.dart';

@JsonSerializable(createToJson: false)
class PictureJsonModel {
  final String title;
  final DateTime date;
  final String explanation;
  final String url;
  final String? hdUrl;
  @JsonKey(name: 'media_type')
  final String mediaType;

  PictureJsonModel({
    required this.title,
    required this.date,
    required this.explanation,
    required this.url,
    this.hdUrl,
    required this.mediaType,
  });

  factory PictureJsonModel.fromJson(dynamic json) =>
      _$PictureJsonModelFromJson(json);

  static List<PictureJsonModel> fromJsonList(dynamic json) {
    if (json is List<dynamic>) {
      var models = json.map((i) => PictureJsonModel.fromJson(i)).toList();
      return models.where((model) => model.mediaType == 'image').toList();
    }

    return [PictureJsonModel.fromJson(json)];
  }

  PictureLocalModel toLocalModel() {
    return PictureLocalModel(
      title: title,
      date: date,
      explanation: explanation,
      url: url,
      hdUrl: hdUrl,
    );
  }
}
