import 'package:apod_app/modules/picture/data/models/picture_json_model.dart';
import 'package:apod_app/modules/picture/data/models/picture_local_model.dart';
import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/modules/picture/domain/repositories/picture_repository.dart';
import 'package:apod_app/shared/data/http/api_datasource.dart';
import 'package:apod_app/shared/data/local_database/local_datasource.dart';
import 'package:apod_app/shared/helpers/extensions/date_extension.dart';
import 'package:apod_app/shared/helpers/failure/failure.dart';
import 'package:dartz/dartz.dart';

class PictureRepositoryImpl implements PictureRepository {
  final ApiDatasource _apiDatasource;
  final LocalDatasource _localDatasource;
  final pageAmount = 10;

  PictureRepositoryImpl({
    required ApiDatasource apiDatasource,
    required LocalDatasource localDatasource,
  })  : _apiDatasource = apiDatasource,
        _localDatasource = localDatasource;

  @override
  Future<Either<Failure, List<Picture>>> fetchPage(
    int page,
    DateTime? date,
  ) async {
    var params = <String, dynamic>{};
    if (date != null) {
      params = {'date': date.toISO8601Date()};
    } else {
      params = {'count': pageAmount};
    }

    var resApi = await _apiDatasource.get(
      '/planetary/apod',
      PictureJsonModel.fromJsonList,
      params: params,
    );

    return resApi.fold((l) async {
      if (l is WithoutConnectionFailure) {
        var pics = await _fetchPageLocal(page);
        return right(pics);
      }
      return left(l);
    }, (pictures) async {
      if (page == 1) {
        await _localDatasource.removeAll<PictureLocalModel>();
      }

      var picturesLocal = pictures.map((e) => e.toLocalModel()).toList();

      await _localDatasource.insertAll<PictureLocalModel>(picturesLocal);
      var pics = await _fetchPageLocal(page);
      return right(pics);
    });
  }

  Future<List<Picture>> _fetchPageLocal(int page) async {
    var local = await _localDatasource.getAllPaginated<PictureLocalModel>(
        pageAmount, (page - 1) * pageAmount);

    return local.map((e) => e.toDomain()).toList();
  }
}
