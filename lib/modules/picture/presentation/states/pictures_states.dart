import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/shared/helpers/failure/failure.dart';
import 'package:apod_app/shared/helpers/states/state_result.dart';

class HomeSearchEmptyStateResult extends StateResult {}

class LoadingPagePicturesStateResult extends SuccessStateResult<List<Picture>> {
  LoadingPagePicturesStateResult(super.data);
}

class ErrorPagePicturesStateResult extends SuccessStateResult<List<Picture>> {
  Failure failure;

  ErrorPagePicturesStateResult({
    required this.failure,
    required List<Picture> data,
  }) : super(data);
}
