import 'package:apod_app/modules/picture/data/repositories/picture_repository_impl.dart';
import 'package:apod_app/modules/picture/domain/entities/paginator.dart';
import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/modules/picture/domain/repositories/picture_repository.dart';
import 'package:apod_app/modules/picture/presentation/states/pictures_states.dart';
import 'package:apod_app/shared/data/http/api_datasource_impl.dart';
import 'package:apod_app/shared/data/http/dio/dio_client.dart';
import 'package:apod_app/shared/data/local_database/isar_local_datasource.dart';
import 'package:apod_app/shared/helpers/states/state_result.dart';
import 'package:apod_app/shared/logger/logger.dart';
import 'package:mobx/mobx.dart';

part 'picture_list_store.g.dart';

class PictureListStore = PictureListStoreBase with _$PictureListStore;

abstract class PictureListStoreBase with Store {
  Paginator paginator;
  DateTime? dateSearch;

  final PictureRepository _pictureRepository;

  PictureListStoreBase() //Include DI
      : _pictureRepository = PictureRepositoryImpl(
          apiDatasource: ApiDatasourceImpl(
            dioClient: getDio(),
            logger: LoggerImpl(),
          ),
          localDatasource: IsarLocalDatasource(),
        ),
        paginator = Paginator(
          currentPage: 1,
        );

  @observable
  StateResult state = DefaultStateResult();

  List<Picture> pictures = [];

  @action
  void setState(StateResult value) => state = value;

  Future<void> fetchFirstPage() {
    return fetchPage(Paginator.fromPage(1));
  }

  void fetchSearch(DateTime? searchValue) {
    dateSearch = searchValue;
    fetchPage(Paginator.fromPage(1));
  }

  void fetchNextPage() {
    fetchPage(paginator.nextPage());
  }

  Future<void> fetchPage(Paginator page) async {
    if (state is LoadingStateResult ||
        state is LoadingPagePicturesStateResult) {
      return;
    }

    if (page.isFirstPage()) {
      setState(LoadingStateResult());
      pictures.clear();
    } else {
      setState(LoadingPagePicturesStateResult(pictures));
    }

    var res = await _pictureRepository.fetchPage(page.currentPage, dateSearch);

    var st = res.fold(
      (failure) {
        if (paginator.isFirstPage()) {
          return FailureStateResult(failure);
        }
        return ErrorPagePicturesStateResult(failure: failure, data: pictures);
      },
      (resPage) {
        pictures.addAll(resPage);
        paginator = Paginator(
          currentPage: page.currentPage,
        );
        return SuccessStateResult(pictures);
      },
    );

    setState(st);
  }
}
