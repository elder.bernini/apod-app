import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/modules/picture/presentation/screens/components/error_item.dart';
import 'package:apod_app/modules/picture/presentation/screens/components/list_state_error.dart';
import 'package:apod_app/modules/picture/presentation/screens/components/list_state_loading.dart';
import 'package:apod_app/modules/picture/presentation/screens/components/loading_item.dart';
import 'package:apod_app/modules/picture/presentation/screens/components/picture_item.dart';
import 'package:apod_app/modules/picture/presentation/screens/picture_detail_screen.dart';
import 'package:apod_app/modules/picture/presentation/states/pictures_states.dart';
import 'package:apod_app/modules/picture/presentation/stores/picture_list_store.dart';
import 'package:apod_app/shared/helpers/states/state_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class PictureListScrren extends StatefulWidget {
  const PictureListScrren({super.key});

  @override
  State<PictureListScrren> createState() => _PictureListScrrenState();
}

class _PictureListScrrenState extends State<PictureListScrren> {
  final _store = PictureListStore();
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(_scrollListener);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _store.fetchFirstPage();
    });
  }

  @override
  void dispose() {
    super.dispose();

    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Astronomy Picture of the Day'),
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        ),
        body: Observer(builder: (context) {
          var state = _store.state;

          if (state is FailureStateResult) {
            return ListStateError(
              failure: state.failure,
              onRetry: _store.fetchFirstPage,
            );
          }

          if (state is SuccessStateResult) {
            return _stateSuccess(state);
          }

          return const ListStateLoading();
        }),
      ),
    );
  }

  void _scrollListener() {
    if (_scrollController.offset >=
        _scrollController.position.maxScrollExtent) {
      _store.fetchNextPage();
    }
  }

  Widget _stateSuccess(SuccessStateResult state) {
    var items = state.data as List<Picture>;

    return RefreshIndicator(
      onRefresh: _store.fetchFirstPage,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 8),
          Center(
            child: ElevatedButton.icon(
              onPressed: _selectDate,
              icon: const Icon(Icons.search_rounded),
              label: const Text('Choose a date to filter'),
            ),
          ),
          const SizedBox(height: 16),
          Expanded(
            child: ListView.builder(
              controller: _scrollController,
              shrinkWrap: true,
              itemCount: items.length,
              itemBuilder: (context, index) {
                var item = PictureItem(
                  picture: items[index],
                  onTapItem: _goToDetail,
                );

                if ((index + 1) == items.length) {
                  if (state is LoadingPagePicturesStateResult) {
                    return Column(
                      children: [
                        item,
                        const LoadingItem(),
                      ],
                    );
                  }
                  if (state is ErrorPagePicturesStateResult) {
                    return Column(
                      children: [
                        item,
                        ErrorItem(
                          onRetry: () => _store.fetchPage(_store.paginator),
                        ),
                      ],
                    );
                  }
                }

                return item;
              },
            ),
          )
        ],
      ),
    );
  }

  _selectDate() async {
    var date = await showDatePicker(
      context: context,
      firstDate: DateTime(1995, 6, 16),
      lastDate: DateTime.now(),
    );

    _store.fetchSearch(date);
  }

  _goToDetail(Picture pic) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => PictureDetailScreen(picture: pic)),
    );
  }
}
