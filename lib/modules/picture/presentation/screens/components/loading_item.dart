import 'package:flutter/material.dart';

class LoadingItem extends StatelessWidget {
  const LoadingItem({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(bottom: 32, top: 16),
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
