import 'package:apod_app/shared/design/typography/text_style.dart';
import 'package:apod_app/shared/helpers/failure/failure.dart';
import 'package:flutter/material.dart';

class ListStateError extends StatelessWidget {
  final Failure failure;
  final VoidCallback onRetry;

  const ListStateError({
    super.key,
    required this.failure,
    required this.onRetry,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Oops..',
            style: pRegular,
          ),
          Text(
            failure.message,
            style: pRegular,
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 24,
          ),
          FilledButton(
            onPressed: onRetry,
            child: const Text('Try again'),
          ),
        ],
      ),
    );
  }
}
