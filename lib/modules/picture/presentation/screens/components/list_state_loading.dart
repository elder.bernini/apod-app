import 'package:flutter/material.dart';

class ListStateLoading extends StatelessWidget {
  const ListStateLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
