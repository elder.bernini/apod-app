import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/shared/design/typography/text_style.dart';
import 'package:apod_app/shared/helpers/extensions/date_extension.dart';
import 'package:apod_app/shared/helpers/type_defs/function_type_defs.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PictureItem extends StatelessWidget {
  final Picture picture;
  final VoidTypedCallback<Picture> onTapItem;

  const PictureItem({
    super.key,
    required this.picture,
    required this.onTapItem,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTapItem(picture);
      },
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Row(
          children: [
            CachedNetworkImage(
              height: MediaQuery.of(context).size.height / 9,
              width: MediaQuery.of(context).size.width / 2.5,
              imageUrl: picture.url,
              fit: BoxFit.cover,
            ),
            const SizedBox(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    picture.date.toStringFormated(),
                    style: pRegular,
                  ),
                  Text(
                    picture.title,
                    style: p2Regular,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
