import 'package:flutter/material.dart';

class ErrorItem extends StatelessWidget {
  final VoidCallback onRetry;

  const ErrorItem({
    super.key,
    required this.onRetry,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 32, top: 16),
      child: Center(
        child: Column(
          children: [
            const Text('An error has been occurred'),
            ElevatedButton(onPressed: onRetry, child: const Text('Try again'))
          ],
        ),
      ),
    );
  }
}
