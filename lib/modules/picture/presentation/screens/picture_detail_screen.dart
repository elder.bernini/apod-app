import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/shared/design/typography/text_style.dart';
import 'package:apod_app/shared/helpers/extensions/date_extension.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PictureDetailScreen extends StatelessWidget {
  final Picture picture;

  const PictureDetailScreen({
    super.key,
    required this.picture,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Picture detail'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: SingleChildScrollView(
        child: IntrinsicHeight(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CachedNetworkImage(imageUrl: picture.hdUrl ?? picture.url),
              const SizedBox(height: 16),
              Text(
                'Picture of day ${picture.date.toStringFormated()}',
                style: pRegular,
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  'Title: ${picture.title}',
                  style: pRegular,
                ),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  'Explanation: ${picture.explanation}',
                  style: pRegular,
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(height: 16),
            ],
          ),
        ),
      ),
    );
  }
}
