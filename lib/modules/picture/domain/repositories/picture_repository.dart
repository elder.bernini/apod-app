import 'package:apod_app/modules/picture/domain/entities/picture.dart';
import 'package:apod_app/shared/helpers/failure/failure.dart';
import 'package:dartz/dartz.dart';

abstract class PictureRepository {
  Future<Either<Failure, List<Picture>>> fetchPage(int page, DateTime? date);
}
