class Paginator {
  final int currentPage;
  final int firstPage = 1;

  Paginator({
    required this.currentPage,
  });

  bool isCurrentPage(int page) => page == currentPage;
  bool isFirstPage() => currentPage == 1;

  Paginator nextPage() {
    return Paginator(
      currentPage: currentPage + 1,
    );
  }

  factory Paginator.fromPage(int page) => Paginator(currentPage: page);
}
