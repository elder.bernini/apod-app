class Picture {
  final String title;
  final DateTime date;
  final String explanation;
  final String url;
  final String? hdUrl;

  Picture({
    required this.title,
    required this.date,
    required this.explanation,
    required this.url,
    this.hdUrl,
  });
}
