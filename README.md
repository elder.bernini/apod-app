# Astronomy Picture of the Day App

A test project developed using [Flutter](https://flutter.dev/) framework.<br/>
The app consumes [APOD api](https://api.nasa.gov/), one of the most popular websites at NASA is the [Astronomy Picture of the Day](https://apod.nasa.gov/apod/astropix.html), and show the pictures and the related data.

_Important:_ the project runs only in Android.

## Getting Started

This app needs a Flutter environment

1. [Install Flutter](https://flutter.dev/docs/get-started/install)
2. [Install VS Code](https://code.visualstudio.com/)
3. Clone this repository<br/>
   3.1. If you don't have Git, please [install it](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) previously

```bash
git clone https://gitlab.com/elder.bernini/apod-app.git
```

4. Open the project folder and run the scripts bellow to install the dependencies

```bash
flutter clean
flutter pub get
flutter pub run build_runner build --delete-conflicting-outputs

#Or use in one line script:
#Powershell
flutter clean; flutter pub get; flutter pub run build_runner build --delete-conflicting-outputs;

#Prompt
flutter clean && flutter pub get && flutter pub run build_runner build --delete-conflicting-outputs
```

5. To use the APOD api you need a key, [generate your api key](https://api.nasa.gov/#signUp) and change the variable `apiKey` in the file `lib\shared\data\http\api_constans.dart`. The default api key allows a limited number of requests, it is recommended to change for your api key before to run.

## Run

To run the project you will need an [Android environment](https://docs.flutter.dev/get-started/install/windows/mobile?tab=first-start#set-up-the-android-emulator), follow the steps to setup it. <br/>

Inside the project folder run the script bellow to build and run the project in the emulator

```bash
flutter run
```

Alternatively you can open the project folder in one IDE like VS Code or Android Studio and run from the IDE. [See more](https://docs.flutter.dev/get-started/test-drive)
